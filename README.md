# ECILA

Welcome to ECILA

# Table of Contents
1. [Introduction](#introduction)
2. [This Repository](#this-repository)
3. [Official Discord Server](#official-discord-server)
4. [Channel Types](#channel-types)
5. [Bot Behavior](#bot-behavior)
6. [User Commands and Usages](#user-commands-and-usages)
7. [Bot and AI Personality Creation](#bot-and-ai-personality-creation)

## Introduction

ECILA is a discord chatbot application that allows to create, edit and host one or many discord AI bots.  
It mainly uses NovelAI as the backend for AI generation, with possible KoboldAI alternative backend support (experimental, only colab TPU support right now).  

ECILA is the fourth complete rewriting of ALICE (previous version available [here](https://gitlab.com/nolialsea/alicebot)), the original chatbot project, that wasn't more than a simple AI Dungeon script back in September 2020.  
The project itself keeps evolving and adapting to current events since then, changing platforms and backends as new and better stuff appears.

## This Repository

Here will be hosted the latest code of the project, as well as its documentation.  
Contributions through Merge Requests or Issues is allowed, even encouraged for everyone!  
Code is far from perfect and always evolving, but I'll try to make it easier for contributions as it goes.

## Official discord server

There is an official discord server currently named "AliceBot" (soon to be renamed into ECILA) that you can join [right here](https://discord.gg/WpJxfXtx4e).  
A lot of AI chatbots are hosted here by myself, some are official and maintained by me, others are community created, either on "Open Bots" or "Patreon Bots", bots provided as a reward when you support me!  
Feel free to pop in there and meet the community and the bots, there is always ~~weird~~ interesting stuff happening there!

## Channel types

Bots can be talked to in three types of channels:  
- Direct Messages: you can DM any bot and converse with it as you want, with almost all commands enabled
- Dedicated Channel: each bot has one dedicated public channel where it will answer. Commands here are restricted to bot owner and admins
- Multibot Channel: some channels allow multiple bots to be inside and talk to each other. This mode is a bit different than the other ones, and the bots will not behave in the same way (more below) 

## Bot Behavior

Depending on the channel you talk to the bots in, their answer conditions will be handled differently.
There are two kind of behaviors for the bots: mono-bot channels and multi-bot channels.

### Mono bot channel behavior

Mono is the default, when you talk to a bot in its dedicated public channel or in DM.  
In this mode, the bot will always send a message back after some delay (14-18sec) when you talk to it.  
Tip: You can use that delay to send multiple consecutive messages to the bots

### Multi bot channel behavior

Multi is for channels where there are multiple bots (`#general` and `#multibot-xxx`)  
In those channels, the bots won't answer to each message like in mono-bot channels.
Instead, each time they should normally answer, they instead perform a check to determine if they want to answer or not.  
Mentioning the exact name of a bot will always trigger an instant response from it.

## User Commands and Usages

There are several commands implemented for you to get the best out of the conversation.

### Emoji Commands

The most common tools are designated as "Emoji Commands": there are emoji reactions that you can put on any bot message to perform various actions.  

- Retry this message: 🔄 (`:arrows_counterclockwise:`) will completely regenerate the selected message and replace its content
- Continue this message: ▶️(`:arrow_forward:`) will generate a continuation of the selected message and edit the content to add the generated result to it
- Speak again, sending a new message: ⏩ (`:fast_forward:`) will make the bot send a new message
- Delete message: ❎ (`:negative_squared_cross_mark:`) will delete this message

### Text Commands

Some commands don't have an emoji command associated and must be written as text:

- Edit a bot message content
  - `!edit Blah` will replace the last bot's message text content by `Blah`, then remove your message (doesn't remove your message in DMs though, bots can't remove your messages in DMs)
  - `!edit 56645649844545 Blah` will replace the text of the bot message with the ID `56645649844545` by `Blah` (right click on a message or long press on mobile to get the message ID **after enabling developer mode**)
- Impersonate a bot (make it say whatever you want)
  - `!impersonate Blah` will make the bot send a message containing `Blah`, then remove your message (doesn't remove your command message in DMs)
- Finish a message (write the beginning and let the AI complete the message)
  - `!finish Blah` will make the bot edit its last message and replace the content with `Blah` plus a completion of the message by the AI
  - `!finish 461521651484 Blah` will make the bot edit the message with id `461521651484` and replace the content with `Blah` plus a completion of the message by the AI
- Change your username in a channel
  - `!alias Blah` will make all the bots inside the current channel see you as `Blah`
  - `!alias` will output your alias (in case you don't remember it)
  - **Keep in mind that it only affects the current channel or DM channel**
  - **Warning: it's not suitable to switch between multiple characters, there is another, better option for that**
- Impersonate someone else (this is suitable to switch between multiple characters)
  - `(as Blah) Hello!` will make the bot see the message `Hello!` with the author named `Blah` instead of your username/alias
  - You can use this to integrate new characters into the conversation
  - **TIP: You can also combine it with the `!finish` commmand:**
    - `!finish (as Blah) Hello,` will make the bot generate the end of the message `Hello,` from the perspective of the character `Blah`


## Bot and AI Personality Creation

It's possible to create your own bots and ~~try to~~ give them the personality you want!  
There are three ways to get your own bots:
- Open Bots
  -Those are community bots, all available to be edited by anyone at any time. There are currently 20 for you to customize, with more coming along the way.
- Patreon Bots
  - As a reward for financially supporting me, each Patreon will be able to activate its own dedicated bot that only them can edit!
- Hosting your own bots
  - Finally, it will soon be possible to host the application yourself, using your own NovelAI account or KoboldAI colab as backend
  - A simple executable will be available for everyone to start the bots and configure them however you want, for whatever server you can (you need to have the proper permissions to add the bot to a discord server)

Whichever option you choose, the commands to edit and customize bots will be the same.  
But before we dive into those commands, some explanations are needed regarding what a bot is and how it works.

### Bot Structure

Each bot and its personality are defined by several things:
- Avatar: Just the bot's discord avatar used as pfp (needs to be an image URL)
- Username: The name of the bot. Bots will always answer when their name is mentioned
- Context: A bit of text that the AIs always remember, usually used to define the bot personality and its current setting as SBF (Square Bracket Format, more on that later)
- Memory: Memories can be managed by users during the conversation, acting as a short or medium term memory
- Introduction: A sentence that defines how the bot is supposed to talk. They will always remember that intro to minimize drift in writing style
- Conversation history: Bots can see in average between 40 and 80 message back, keep in mind that it can affect their personality too!

Memory and conversation history being tied to the channel, this leaves 4 commands to edit the bots:

- `!avatar https://your.image.url/image.png` Sets the avatar of the bot (only works with URLs)
  - `!avatar` Shows the URL of the bot's avatar
- `!username BlahBot` Sets the username of the bot to `BlahBot`
  - `!username` Shows the username of the bot (was that really necessary? Idk but it's here)
- `!context [ Blah blah blah ]` Sets the context of the bot
  - `!context` Shows the context of the bot
  - **Notice the square brackets.** They are a primordial part of the AI, and indicate that the text is **contextual**
- `!introduction Hello my name is BlahBot!` Sets the introduction of the bot to `Hello my name is BlahBot!`
  - `!introduction` Shows the introduction of the bot
  
